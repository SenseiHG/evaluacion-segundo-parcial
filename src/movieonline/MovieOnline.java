
package movieonline;

import java.util.Scanner;


public class MovieOnline {
    
    static Scanner entrada = new Scanner(System.in);
    
    public static void main(String[] args) {
        
        Pelicula peliculas[] = new Pelicula[4];
        peliculas[0] = new Pelicula("Fast & Furious", " Accion");
        peliculas[1] = new Pelicula("RED", "Accion");
        peliculas[2] = new Pelicula("Coco", "Ciencia Ficcion");
        peliculas[3] = new Pelicula("DEPREDADOR", "Terror");
        
        for (int i = 0; i < 4; i++) {
            for(peliculas[i].getGenero().equals("Terror"))
                peliculas[i].MostrarPelicula();
        }
    }
    
}
