
package movieonline;

import java.util.List;


class Pelicula {

    private String Nombre;
    private String Genero;
    private List<PeliculaFav> Pelifavorita;

    public Pelicula(String Nombre, String Genero) {
        this.Nombre = Nombre;
        this.Genero= Genero;
    }

    public String getNombre() {
        return Nombre;
    }

    public void setNombre(String Nombre) {
        this.Nombre = Nombre;
    }

    public String getGenero() {
        return Genero;
    }

    public void setGenero(String Genero) {
        this.Genero = Genero;
    }

    
    void MostrarPelicula() {
        System.out.println("pelicula:" + Nombre);
        System.out.println("Genero:" + Genero);
        System.out.println(Pelifavorita);
       }

    public List<PeliculaFav> getPelifavorita() {
        return Pelifavorita;
    }

    public void setPelifavorita(List<PeliculaFav> Pelifavorita) {
        this.Pelifavorita = Pelifavorita;
    }


}
